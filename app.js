'use strict';

const setConfigs = require('./src/configs/setConfigs');
const init = require('./src/index');
const app = require('express')();

setConfigs(app);
init(app);

app.listen(app.get('port'), () => {
    console.log('Server has been started on port:', app.get('port'));
});

// var serverless = require('serverless-http');
// module.exports.handler = serverless(app);
