'use strict';

const router = require('express').Router();
const AuthController = require('../api/controllers/AuthController');

router.get('/', (req, res) => res.json({ message: 'Welcome to the API' }));
router.post('/login', AuthController.login);
router.post('/todos', AuthController.verify, (req, res) => {
    res.json({ code: 200, message: 'Task created', user: res.locals.user })
});

module.exports = router;
