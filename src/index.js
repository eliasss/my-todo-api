'use strict';

const apiRoutes = require('./routes/api');

module.exports = (app) => {
    app.use('/api', apiRoutes);
};
