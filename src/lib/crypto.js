const crypto = require('crypto');

module.exports.encrypt = (data, key) => {
    var cipher = crypto.createCipher('aes-256-cbc', key)
    var crypted = cipher.update(data, 'utf8', 'hex')
    crypted += cipher.final('hex');
    return crypted;
}

module.exports.decrypt = (data, key) => {
    var decipher = crypto.createDecipher('aes-256-cbc', key)
    var dec = decipher.update(data, 'hex', 'utf8')
    dec += decipher.final('utf8');
    return dec;
}