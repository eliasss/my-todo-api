const jwt = require('jsonwebtoken');

module.exports.sign = (data, key, opts = {}) => {
    Object.assign({ expiresIn: '6h' }, opts);

    return new Promise((resolve, reject) => {
        jwt.sign(data, key, opts, (err, token) => {
            if (err) {
                reject(err);
            } else {
                resolve(token);
            }
        });
    })
}


module.exports.verify = (token, key) => {
    return new Promise((resolve, reject) => {
        jwt.verify(token, key, (err, data) => {
            if (err) {
                reject(err);
            } else {
                resolve(data);
            }
        });
    })
}