const buildResponseBody = (code, message = '', result = {}) => {
    return {
        code, message, result
    }
}

module.exports = {
    forbidden: (res, message, result) => {
        return res.status(403).json(
            buildResponseBody(403, message, result)
        );
    },
    success: (res, message, result) => {
        return res.status(200).json(
            buildResponseBody(200, message, result)
        );
    },
}
