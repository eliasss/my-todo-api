'use strict'

const ENV = process.env.NODE_ENV || 'dev';
const CONFIG = require(`./config.${ENV}`);
const bodyParser = require('body-parser');

module.exports = (app) => {
    app.use(bodyParser.urlencoded({ extended: false }));
    app.use(bodyParser.json());
    app.use((req, res, next) => {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        next();
    });

    app.set('port', CONFIG.port);
    
    return app;
}