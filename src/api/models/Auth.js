'use strict';

const crypto = require('../../lib/crypto');
const jwt = require('../../lib/jwt');
const TOKEN_KEY = 'dae19b26-6825-4b5c-806c-468051d3ea90';

const createHash = (username, password) => {
    const data = `${username}##${password}`
    return crypto.encrypt(data, password);
}

class Auth {
    autenticate(username, password) {
        try {
            const user = {
                id: 2,
                username: 'elias@email.com',
                password: 'bc0ebeeb9583c7ad287d4280682d95719955aeb310a71f1b0bf74a4011c50a7e'
            };

            crypto.decrypt(user.password, password);
            return jwt.sign(user, TOKEN_KEY);

        } catch(error) {
            console.log(error);
            return Promise.reject();
        }
    }

    autorize(token) {
        return jwt.verify(token, TOKEN_KEY);
    }
}

module.exports = Auth;
