'use strict'

const response = require('../../lib/response');
const Auth = require('../models/Auth');

class AuthController {

    static login(req, res) {
        const { username, password } = req.body;

        if (!username) return response.forbidden(res, 'Username is not defined');
        if (!password) return response.forbidden(res, 'Password is not defined');

        return new Auth().autenticate(username, password)
            .then(token => response.success(res, 'Login done with success', { token }))
            .catch(() => response.forbidden(res, 'Invalid username or password'))
    }

    static verify(req, res, next) {
        const token = req.headers['authorization'];
        if (!token) return response.forbidden(res, 'The Authorization header is not defined');

        return new Auth().autorize(token)
            .then(data => {
                res.locals.user = data;
                next()
            })
            .catch(() => response.forbidden(res, 'Invalid token'))
    }
}

module.exports = AuthController;
